## 線形回帰


# 1.1 線形回帰

inner.prod = function(x, y) return(sum(x * y))
linear = function(X, y) {
  n = nrow(X); p = ncol(X)
  X = as.matrix(X); x.bar = array(dim = p); for (j in 1:p) x.bar[j] = mean(X[, j])
  for (j in 1:p) X[, j] = X[, j] - x.bar[j]          ## Xの中心化
  y = as.vector(y); y.bar = mean(y); y = y - y.bar   ## yの中心化
  beta = as.vector(solve(t(X) %*% X) %*% t(X) %*% y)
  beta.0 = y.bar - sum(x.bar * beta)
  return(list(beta = beta, beta.0 = beta.0))
}


# 1.2 劣微分

curve(x ^ 2 - 3 * x + abs(x), -2, 2, main = "y = x^2 - 3x + |x|")
points(1, -1, col = "red", pch = 16)
curve(x ^ 2 + x + 2 * abs(x), -2, 2, main = "y = x^2 + x + 2|x|")
points(0, 0, col = "red", pch = 16)


# 1.3 Lasso

soft.th = function(lambda, x) return(sign(x) * pmax(abs(x) - lambda, 0))
curve(soft.th(5, x), -10, 10, main = "soft.th(lambda, x)")
segments(-5, -4, -5, 4, lty = 5, col = "blue"); segments(5, -4, 5, 4, lty = 5, col = "blue")
text(-0.2, 1, "lambda = 5", cex = 1.5, col = "red")

linear.lasso = function(X, y, lambda = 0, beta = rep(0, ncol(X))) {
  n = nrow(X); p = ncol(X)
  res = centralize(X, y)   ## 中心化（下記参照）
  X = res$X; y = res$y
  eps = 1; beta.old = beta
  while (eps > 0.001) {    ## このループの収束を待つ
    for (j in 1:p) {
      r = y - as.matrix(X[, -j]) %*% beta[-j]
      beta[j] = soft.th(lambda, sum(r * X[, j]) / n) / (sum(X[, j] * X[, j]) / n) 
    }
    eps = max(abs(beta - beta.old)); beta.old = beta
  }
  beta = beta / res$X.sd   ## 各変数の係数を正規化前のものに戻す
  beta.0 = res$y.bar - sum(res$X.bar * beta)
  return(list(beta = beta, beta.0 = beta.0))
}

centralize = function(X, y, standardize = TRUE) {
  X = as.matrix(X)
  n = nrow(X); p = ncol(X)
  X.bar = array(dim = p)          ## Xの各列の平均
  X.sd = array(dim = p)           ## Xの各列の標準偏差
  for (j in 1:p) {
    X.bar[j] = mean(X[, j])
    X[, j] = (X[, j] - X.bar[j])  ## Xの各列の中心化
    X.sd[j] = sqrt(var(X[, j]))
    if (standardize == TRUE) X[, j] = X[, j] / X.sd[j]   ## Xの各列の標準化
  }
  if (is.matrix(y)) {     ## yが行列の場合
    K = ncol(y)
    y.bar = array(dim = K)        ## yの平均
    for (k in 1:K) {
      y.bar[k] = mean(y[, k])
      y[, k] = y[, k] - y.bar[k]  ## yの中心化
    }
  } else {                        ## yがベクトルの場合
    y.bar = mean(y)
    y = y - y.bar
  }
  return(list(X = X, y = y, X.bar = X.bar, X.sd = X.sd, y.bar = y.bar))
}

df = read.table("crime.txt")
x = df[, 3:7]; y = df[, 1]; p = ncol(x); lambda.seq = seq(0, 200, 0.1)
plot(lambda.seq, xlim = c(0, 200), ylim = c(-10, 20), xlab = "lambda", ylab = "beta",
     main = "各 lambda についての各係数の値", type = "n", col = "red")
r=length(lambda.seq)
coef.seq = array(dim=c(r,p))
for (i in 1:r) coef.seq[i,] = linear.lasso(x, y, lambda.seq[i])$beta
for(j in 1:p){
　　　par(new = TRUE); lines(lambda.seq, coef.seq[,j], col = j)
}
legend("topright",
       legend = c("警察への年間資金", "25 歳以上で高校を卒業した人の割合",
                  "16-19 歳で高校に通っていない人の割合", "18-24 歳で大学生の割合",
                  "25 歳以上で 4 年制大学を卒業した人の割合"),
       col = 1:p, lwd = 2, cex = .8)

warm.start = function(X, y, lambda.max = 100) {
  dec = round(lambda.max / 50); lambda.seq = seq(lambda.max, 1, -dec)
  r = length(lambda.seq); p = ncol(X); coef.seq = matrix(nrow = r, ncol = p)
  coef.seq[1, ] = linear.lasso(X, y, lambda.seq[1])$beta
  for (k in 2:r) coef.seq[k, ] = linear.lasso(X, y, lambda.seq[k], coef.seq[(k - 1), ])$beta
  return(coef.seq)
}

crime = read.table("crime.txt"); X = crime[, 3:7]; y = crime[, 1] 
coef.seq = warm.start(X, y, 200)
p = ncol(X); lambda.max = 200; dec = round(lambda.max / 50)
lambda.seq = seq(lambda.max, 1, -dec)
plot(log(lambda.seq), coef.seq[, 1], xlab = "log(lambda)", ylab = "係数",
     ylim = c(min(coef.seq), max(coef.seq)), type = "n")
for (j in 1:p) lines(log(lambda.seq), coef.seq[, j], col = j)

library(glmnet); library(MASS)
df = Boston; x = as.matrix(df[, 1:13]); y = df[, 14]
fit = glmnet(x, y); plot(fit, xvar = "lambda", main = "BOSTON")


# 1.4 Ridge

ridge = function(X, y, lambda = 0) {
  X = as.matrix(X); p = ncol(X); n = length(y)
  res = centralize(X, y); X = res$X; y = res$y
  ## Ridgeの処理は，次の1行のみ
  beta = drop(solve(t(X) %*% X + n * lambda * diag(p)) %*% t(X) %*% y)
  beta = beta / res$X.sd  ## 各変数の係数を正規化前のものに戻す
  beta.0 = res$y.bar - sum(res$X.bar * beta)
  return(list(beta = beta, beta.0 = beta.0))
}

df = read.table("crime.txt")
x = df[, 3:7]; y = df[, 1]; p = ncol(x); lambda.seq = seq(0, 100, 0.1)
plot(lambda.seq, xlim = c(0, 100), ylim = c(-10, 20), xlab = "lambda", ylab = "beta",
     main = "各 lambda についての各係数の値", type = "n", col = "red") 
r=length(lambda.seq)
coef.seq = array(dim=c(r,p))
for (i in 1:r) coef.seq[i,] = ridge(x, y, lambda.seq[i])$beta
for(j in 1:p){
　　　par(new = TRUE); lines(lambda.seq, coef.seq[,j], col = j)
}
legend("topright",
       legend = c("警察への年間資金", "25 歳以上で高校を卒業した人の割合",
                  "16-19 歳で高校に通っていない人の割合", "18-24 歳で大学生の割合",
                  "25 歳以上で 4 年制大学を卒業した人の割合"),
       col = 1:p, lwd = 2, cex = .8)

crime = read.table("crime.txt")
X = crime[, 3:7]
y = crime[, 1]
linear(X, y)

ridge(X, y)

ridge(X, y, 200)


# 1.5 LassoとRidgeを比較して

R2 = function(x, y) {
  y.hat = lm(y ~ x)$fitted.values; y.bar = mean(y)
  RSS = sum((y - y.hat) ^ 2); TSS = sum((y - y.bar) ^ 2)
  return(1 - RSS / TSS)
}
vif = function(x) {
  p = ncol(x); values = array(dim = p)
  for (j in 1:p) values[j] = 1 / (1 - R2(x[, -j], x[, j]))
  return(values)
}
library(MASS); x = as.matrix(Boston); vif(x)

n = 500; x = array(dim = c(n, 6)); z = array(dim = c(n, 2))
for (i in 1:2) z[, i] = rnorm(n)
y = 3 * z[, 1] - 1.5 * z[, 2] + 2 * rnorm(n)
for (j in 1:3) x[, j] = z[, 1] + rnorm(n) / 5
for (j in 4:6) x[, j] = z[, 2] + rnorm(n) / 5
glm.fit = glmnet(x, y); plot(glm.fit)
legend("topleft", legend = c("X1", "X2", "X3", "X4", "X5", "X6"), col = 1:6, lwd = 2, cex = .8)


# 1.6 elasticネット

linear.lasso = function(X, y, lambda = 0, beta = rep(0, ncol(X)), alpha = 1) {  #
  X = as.matrix(X); n = nrow(X); p = ncol(X); X.bar = array(dim = p)
  for (j in 1:p) {X.bar[j] = mean(X[, j]); X[, j] = X[, j] - X.bar[j]}
  y.bar = mean(y); y = y - y.bar
  scale = array(dim = p)
  for (j in 1:p) {scale[j] = sqrt(sum(X[, j] ^ 2) / n); X[, j] = X[, j] / scale[j]}
  eps = 1; beta.old = beta
  while (eps > 0.001) {
    for (j in 1:p) {
      r = y - as.matrix(X[, -j]) %*% beta[-j]
      beta[j] = soft.th(lambda * alpha,          ## 
      sum(r * X[, j]) / n) / (sum(X[, j] * X[, j]) / n + 
        lambda * (1 - alpha))                    ##
    }
    eps = max(abs(beta - beta.old)); beta.old = beta
  }
  for (j in 1:p) beta[j] = beta[j] / scale[j]
  beta.0 = y.bar - sum(X.bar * beta)
  return(list(beta = beta, beta.0 = beta.0))
}


# 1.7 lambda$の値の設定

library(glmnet)
df = read.table("crime.txt"); X = as.matrix(df[, 3:7]); y = as.vector(df[, 1])
cv.fit = cv.glmnet(X, y); plot(cv.fit)
lambda.min = cv.fit$lambda.min; lambda.min

n = 500; x = array(dim = c(n, 6)); z = array(dim = c(n, 2))
for (i in 1:2) z[, i] = rnorm(n)
y = 3 * z[, 1] - 1.5 * z[, 2] + 2 * rnorm(n)
for (j in 1:3) x[, j] = z[, 1] + rnorm(n) / 5
for (j in 4:6) x[, j] = z[, 2] + rnorm(n) / 5
best.score = Inf
for (alpha in seq(0, 1, 0.01)) {
  res = cv.glmnet(x, y, alpha = alpha)
  lambda = res$lambda.min; min.cvm = min(res$cvm)
  if (min.cvm < best.score) {alpha.min = alpha; lambda.min = lambda; best.score = min.cvm}
}

alpha.min

lambda.min

glmnet(x, y, alpha = alpha.min, lambda = lambda.min)$beta

glm.fit = glmnet(x, y, alpha = alpha.min)
plot(glm.fit)
