# 第2章 一般化線形回帰

***************
#### 第1章より
```{r}
soft.th <- function(lambda, x) {
  return(sign(x) * pmax(abs(x) - lambda, 0))
}
```

```{r}
linear.lasso <- function(X, y, lambda = 0, beta = rep(0, ncol(X))) {
  n <- nrow(X)
  p <- ncol(X)
  res <- centralize(X, y)   ## 中心化（下記参照）
  X <- res$X
  y <- res$y
  eps <- 1
  beta.old <- beta
  while (eps > 0.001) {    ## このループの収束を待つ
    for (j in 1:p) {
      r <- y - as.matrix(X[, -j]) %*% beta[-j]
      beta[j] <- soft.th(lambda, sum(r * X[, j]) / n) / (sum(X[, j] * X[, j]) / n) 
    }
    eps <- max(abs(beta - beta.old))
    beta.old <- beta
  }
  beta <- beta / res$X.sd   ## 各変数の係数を正規化前のものに戻す
  beta.0 <- res$y.bar - sum(res$X.bar * beta)
  return(list(beta = beta, beta.0 = beta.0))
}
```

```{r}
centralize <- function(X, y, standardize = TRUE) {
  X <- as.matrix(X)
  n <- nrow(X)
  p <- ncol(X)
  X.bar <- array(dim = p)          ## Xの各列の平均
  X.sd <- array(dim = p)           ## Xの各列の標準偏差
  for (j in 1:p) {
    X.bar[j] <- mean(X[, j])
    X[, j] <- (X[, j] - X.bar[j])  ## Xの各列の中心化
    X.sd[j] <- sqrt(var(X[, j]))
    if (standardize == TRUE) 
      X[, j] <- X[, j] / X.sd[j]   ## Xの各列の標準化
  }
  if (class(y) == "matrix") {      ## yが行列の場合
    K <- ncol(y)
    y.bar <- array(dim = K)        ## yの平均
    for (k in 1:K) {
      y.bar[k] <- mean(y[, k])
      y[, k] <- y[, k] - y.bar[k]  ## yの中心化
    }
  } else {                         ## yがベクトルの場合
    y.bar <- mean(y)
    y <- y - y.bar
  }
  return(list(X = X, y = y, X.bar = X.bar, X.sd = X.sd, y.bar = y.bar))
}
```
***************

## 2.1 線形回帰のLassoの一般化

```{r}
W.linear.lasso <- function(X, y, W, lambda = 0) {
  n <- nrow(X)
  p <- ncol(X)
  X.bar <- array(dim = p)
  for (k in 1:p) {
    X.bar[k] <- sum(W %*% X[, k]) / sum(W)
    X[, k] <- X[, k] - X.bar[k]
  }
  y.bar <- sum(W %*% y) / sum(W)
  y <- y - y.bar
  L <- chol(W)
  # L <- sqrt(W)
  u <- as.vector(L %*% y)
  V <- L %*% X
  beta <- linear.lasso(V, u, lambda)$beta
  beta.0 <- y.bar - sum(X.bar * beta)
  return(c(beta.0, beta))
}
```

## 2.2 2値のロジスティック回帰

### 例11
```{r}
f <- function(x) {
  return(exp(beta.0 + beta * x) / (1 + exp(beta.0 + beta * x)))
}
beta.0 <- 0
beta.seq <- c(0, 0.2, 0.5, 1, 2, 10)
m <- length(beta.seq)
beta <- beta.seq[1]
plot(f, xlim = c(-10, 10), ylim = c(0, 1), xlab = "x", ylab = "y",
     col = 1, main = "ロジスティック曲線")
for (i in 2:m) {
  beta <- beta.seq[i]
  par(new = TRUE)
  plot(f, xlim = c(-10, 10), ylim = c(0, 1), xlab = "", ylab = "", axes = FALSE, col = i)
}
legend("topleft", legend = beta.seq, col = 1:length(beta.seq), lwd = 2, cex = .8)
par(new = FALSE)
```

### 例12
```{r}
## データ生成
N <- 100
p <- 2
X <- matrix(rnorm(N * p), ncol = p)
X <- cbind(rep(1, N), X)
beta <- rnorm(p + 1)
y <- array(N)
s <- as.vector(X %*% beta)
prob <- 1 / (1 + exp(s)) 
for (i in 1:N) {
  if (runif(1) > prob[i]) {
    y[i] <- 1
  } else {
    y[i] <- -1
  }
}
beta

## 最尤推定値の計算
beta <- Inf
gamma <- rnorm(p + 1)
while (sum((beta - gamma) ^ 2) > 0.001) {
  beta <- gamma
  s <- as.vector(X %*% beta)
  v <- exp(-s * y)
  u <- y * v / (1 + v)
  w <- v / (1 + v) ^ 2
  z <- s + u / w
  W <- diag(w)
  gamma <- as.vector(solve(t(X) %*% W %*% X) %*% t(X) %*% W %*% z)          ##
  print(gamma)
}
beta  ## 真の値。最尤法でこの値を推定したい
```

```{r}
logistic.lasso <- function(X, y, lambda) {
  p <- ncol(X)
  beta <- Inf
  gamma <- rnorm(p)
  while (sum((beta - gamma) ^ 2) > 0.01) {
    beta <- gamma
    s <- as.vector(X %*% beta)
    v <- as.vector(exp(-s * y))
    u <- y * v / (1 + v)
    w <- v / (1 + v) ^ 2
    z <- s + u / w
    W <- diag(w)
    gamma <- W.linear.lasso(X[, 2:p], z, W, lambda = lambda)
    print(gamma)
  }
  return(gamma)
}
```

### 例13
```{r}
N <- 100
p <- 2
X <- matrix(rnorm(N * p), ncol = p)
X <- cbind(rep(1, N), X)
beta <- rnorm(p + 1)
y <- array(N)
s <- as.vector(X %*% beta)
prob <- 1 / (1 + exp(s))
for (i in 1:N) {
  if (runif(1) > prob[i]) {
    y[i] <- 1
  } else {
    y[i] <- -1
  }
}
#logistic.lasso(X, y, 0)
logistic.lasso(X, y, 0.1)
logistic.lasso(X, y, 0.2)
```

### 例14
```{r}
## データ生成
N <- 1000
p <- 2
X <- matrix(rnorm(N * p), ncol = p)
X <- cbind(rep(1, N), X)
beta <- 10 * rnorm(p + 1)
y <- array(N)
s <- as.vector(X %*% beta)
prob <- 1 / (1 + exp(s)) 
for (i in 1:N) {
  if (runif(1) > prob[i]) {
    y[i] <- 1
  } else {
    y[i] <- -1
  }
}

## パラメータ推定
beta.est <- logistic.lasso(X, y, 0.1)

## 分類処理
for (i in 1:N) {
  if (runif(1) > prob[i]) {
    y[i] <- 1
  } else {
    y[i] <- -1
  }
}
z <- sign(X %*% beta.est)  ## 指数部が正なら+1, 負なら-1と判定する
table(y, z)
```

### 例15
```{r}
library(glmnet)
df <- read.csv("breastcancer.csv")
## ファイル breastcancer.csv をカレントディレクトリにおく
x <- as.matrix(df[, 1:1000])
y <- as.vector(df[, 1001])
cv <- cv.glmnet(x, y, family = "binomial")
cv2 <- cv.glmnet(x, y, family = "binomial", type.measure = "class")
par(mfrow = c(1, 2))
plot(cv)
plot(cv2)
par(mfrow = c(1, 1))
```

```{r}
glm <- glmnet(x, y, lambda = 0.03, family = "binomial")
beta <- drop(glm$beta)
beta[beta != 0]
```

## 2.3 多値のロジスティック回帰

```{r}
multi.lasso <- function(X, y, lambda) {
  X <- as.matrix(X)
  p <- ncol(X)
  n <- nrow(X)
  K <- length(table(y))
  beta <- matrix(1, nrow = K, ncol = p)
  gamma <- matrix(0, nrow = K, ncol = p)
  while (norm(beta - gamma, "F") > 0.1) {
    gamma <- beta
    for (k in 1:K) {
      r <- 0
      for (h in 1:K)
        if (k != h)
          r <- r + exp(as.vector(X %*% beta[h, ]))
      v <- exp(as.vector(X %*% beta[k, ])) / r
      u <- as.numeric(y == k) - v / (1 + v)
      w <- v / (1 + v) ^ 2
      z <- as.vector(X %*% beta[k, ]) + u / w
      beta[k, ] <- W.linear.lasso(X[, 2:p], z, diag(w), lambda = lambda) 
      print(beta[k, ])
    }
    for (j in 1:p) {
      med <- median(beta[, j])
      for (h in 1:K)
        beta[h, j] <- beta[h, j] - med
    }
  }
  return(beta)
}
```

### 例16
```{r}
df <- iris
x <- matrix(0, 150, 4)
for (j in 1:4)
  x[, j] <- df[[j]]
X <- cbind(1, x)
y <- c(rep(1, 50), rep(2, 50), rep(3, 50))
beta <- multi.lasso(X, y, 0.01)
X %*% t(beta)
```

### 例17
```{r}
library(glmnet)
df <- iris
x <- as.matrix(df[, 1:4])
y <- as.vector(df[, 5]) 
n <- length(y)
u <- array(dim = n)
for (i in 1:n) {
  if (y[i] == "setosa") {
    u[i] <- 1
  } else if (y[i] == "versicolor") {
    u[i] <- 2
  } else {
    u[i] <- 3
  }
}
u <- as.numeric(u)
cv <- cv.glmnet(x, u, family = "multinomial")
cv2 <- cv.glmnet(x, u, family = "multinomial", type.measure = "class")
par(mfrow = c(1, 2))
plot(cv)
plot(cv2)
par(mfrow = c(1, 1))
lambda <- cv$lambda.min
result <- glmnet(x, y, lambda = lambda, family = "multinomial")
beta <- result$beta
beta.0 <- result$a0
v <- rep(0, n)
for (i in 1:n) {
  max.value <- -Inf
  for (j in 1:3) {
    value <- beta.0[j] + sum(beta[[j]] * x[i, ])
    if (value > max.value) {
      v[i] <- j
      max.value <- value
    }
  }
}
table(u, v)
```

## 2.4 ポアッソン回帰

```{r}
poisson.lasso <- function(X, y, lambda) {
  beta <- rnorm(p + 1)
  gamma <- rnorm(p + 1)
  while (sum((beta - gamma) ^ 2) > 0.0001) {
    beta <- gamma
    s <- as.vector(X %*% beta)
    w <- exp(s)
    u <- y - w
    z <- s + u / w
    W <- diag(w)
    gamma <- W.linear.lasso(X[, 2:(p + 1)], z, W, lambda)
    print(gamma)
  }
  return(gamma)
}
```

### 例18
```{r}
n <- 10

00
p <- 3
beta <- rnorm(p + 1)
X <- matrix(rnorm(n * p), ncol = p)
X <- cbind(1, X)
s <- as.vector(X %*% beta)
y <- rpois(n, lambda = exp(s))
beta
poisson.lasso(X, y, 0.2)
```

### 例19
```{r}
library(glmnet)
library(MASS)
data(birthwt)
df <- birthwt[, -1]
dy <- df[, 8]
dx <- data.matrix(df[, -8])
cvfit <- cv.glmnet(x = dx, y = dy, family = "poisson", standardize = TRUE)
coef(cvfit, s = "lambda.min")
```

## 2.5 生存時間解析

```{r}
library(survival)
data(kidney)
kidney
y <- kidney$time
delta <- kidney$status
Surv(y, delta)
```

### 例21
```{r}
fit <- survfit(Surv(time, status) ~ disease, data = kidney)
plot(fit, xlab = "時間", ylab = "生存率", col = c("red", "green", "blue", "black"))
legend(300, 0.8, legend = c("その他", "GN", "AN", "PKD"), 
       lty = 1, col = c("red", "green", "blue", "black"))
```

```{r}
cox.lasso <- function(X, y, delta, lambda = lambda) {
  delta[1] <- 1
  n <- length(y)
  w <- array(dim = n)
  u <- array(dim = n)
  pi <- array(dim = c(n, n))
  beta <- rnorm(p)
  gamma <- rep(0, p)
  while (sum((beta - gamma) ^ 2) > 10 ^ {-4}) {
    beta <- gamma
    s <- as.vector(X %*% beta)
    v <- exp(s)
    for (i in 1:n)
      for (j in 1:n)
        pi[i, j] <- v[i] / sum(v[j:n])
    for (i in 1:n) {
      u[i] <- delta[i]
      w[i] <- 0
      for (j in 1:i) {
        if (delta[j] == 1) {
          u[i] <- u[i] - pi[i, j]
          w[i] <- w[i] + pi[i, j] * (1 - pi[i, j])
        }
      }
    }
    z <- s + u / w
    W <- diag(w)
    print(gamma)
    gamma <- W.linear.lasso(X, z, W, lambda = lambda)[-1]
  }
  return(gamma)
}
```

### 例22
```{r}
df <- kidney
index <- order(df$time)
df <- df[index, ]
n <- nrow(df)
p <- 4
y <- as.numeric(df[[2]])
delta <- as.numeric(df[[3]])
X <- as.numeric(df[[4]])
for (j in 5:7)
  X <- cbind(X, as.numeric(df[[j]]))
z <- Surv(y, delta)
cox.lasso(X, y, delta, 0)
cox.lasso(X, y, delta, 0.1)
cox.lasso(X, y, delta, 0.2)
glmnet(X, z, family = "cox", lambda = 0.1)$beta
```

### 例23
```{r}
library(survival)
load("LymphomaData.rda")
attach("LymphomaData.rda")
names(patient.data)
x <- t(patient.data$x)
y <- patient.data$time
delta <- patient.data$status
Surv(y, delta)
```

```{r}
library(ranger)
library(ggplot2)
library(dplyr)
library(ggfortify)
cv.fit <- cv.glmnet(x, Surv(y, delta), family = "cox")
fit2 <- glmnet(x, Surv(y, delta), lambda = cv.fit$lambda.min, family = "cox")
z <- sign(drop(x %*% fit2$beta))
fit3 <- survfit(Surv(y, delta) ~ z)
autoplot(fit3)
mean(y[z == 1])
mean(y[z == -1])
```

